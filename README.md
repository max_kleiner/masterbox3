maXbox
======

****************************************************************
Release Notes maXbox 3.9.9.190 Mai 2015 CODEsign
****************************************************************

Add 28 Units, 1 Tutor, SOAPConn, AVI Res, OLEUtils, ACM

1085 unit uPSI_JvAnimate //JCL 1086 unit uPSI_DBXCharDecoder; //DBX 1087 unit uPSI_JvDBLists; //JCL 1088 unit uPSI_JvFileInfo; //JCL 1089 unit uPSI_SOAPConn; //VCL 1090 unit uPSI_SOAPLinked; //VCL 1091 unit uPSI_XSBuiltIns; //VCL 1092 unit uPSI_JvgDigits; //JCL 1093 unit uPSI_JvDesignUtils; 1094 unit uPSI_JvgCrossTable; 1095 unit uPSI_JvgReport; 1096 unit uPSI_JvDBRichEdit; 1097 unit uPSI_JvWinHelp; 1098 unit uPSI_WaveConverter; 1099 unit uPSI_ACMConvertor; 1100 unit XSBuiltIns_Routines 1101 unit uPSI_ComObjOleDB_utils.pas 1102 unit uPSI_SMScript; 1103 unit uPSI_CompFileIo; 1104 unit uPSI_SynHighlighterGeneral; 1105 unit uPSI_geometry2; 1106 unit uPSI_MConnect 1107 unit uPSI_ObjBrkr; 1108 unit uPSI_uMultiStr; 1109 unit uPSI_WinAPI.pas; 1110 unit uPSI_JvAVICapture; 1111 unit uPSI_JvExceptionForm; 1112 unit uPSI_JvConnectNetwork;

SHA1: maXbox3.exe E2E7D254A4746B2E4DD8AC80FBC4FC151EBD4B2A CRC32: maXbox3.exe 7BFAD6E8


****************************************************************
Release Notes maXbox 3.9.9.88 March 2014
****************************************************************
2 Tutorials 30 Units add, VCL constructors, controls+, unit list

786 uPSI_FileUtil;
787 uPSI_changefind;
788 uPSI_cmdIntf;
789 uPSI_fservice;
790 uPSI_Keyboard;
791 uPSI_VRMLParser,
792 uPSI_GLFileVRML,
793 uPSI_Octree;
794 uPSI_GLPolyhedron,
795 uPSI_GLCrossPlatform;
796 uPSI_GLParticles;
797 uPSI_GLNavigator;
798 uPSI_GLStarRecord;
799 uPSI_TGA;
800 uPSI_GLCanvas;
801 uPSI_GeometryBB;
802 uPSI_GeometryCoordinates;
803 uPSI_VectorGeometry;
804 uPSI_BumpMapping;
805 uPSI_GLTextureCombiners;
806 uPSI_GLVectorFileObjects;
807 uPSI_IMM;
808 uPSI_CategoryButtons;
809 uPSI_ButtonGroup;
810 uPSI_DbExcept;
811 uPSI_AxCtrls;
812 uPSI_GL_actorUnit1;
813 uPSI_StdVCL;
814 unit CurvesAndSurfaces;
815 uPSI_DataAwareMain;

SHA1: Win 3.9.9.88: 119533C0725A9B9B2919849759AA2F6298EBFF28